# * preliminary targets
.ONESHELL:
SHELL = /bin/bash

include settings.mk

.PHONY: clone_gitlab_arbetsformedlingen
GHORGGITLAB = --scm=gitlab --token=$(GHORGGITLABTOKEN)  --clone-wiki
GHORG=~/bin/ghorg
#we dont want -infra repos, not private repos either, since end result is meant to be public
GHORGFLAGS = --skip-archived --preserve-dir  --protocol=https --no-clean  --path=$(CURDIR)/mirror --config ./ghorg.yaml       --concurrency 1
# --path=$(GHORGDIR)

check-tools:
	@which ghorg >/dev/null 2>&1 || { echo >&2 "The 'ghorg' tool is required but it's not installed. Please install it and try again."; exit 1; }
	@which podman >/dev/null 2>&1 || { echo >&2 "The 'podman' tool is required but it's not installed. Please install it and try again."; exit 1; }
	@which bb >/dev/null 2>&1 || { echo >&2 "The 'bb(babashka)' tool is required but it's not installed. Please install it and try again."; exit 1; }
	@which hugo >/dev/null 2>&1 || { echo >&2 "The 'hugo' tool is required but it's not installed. Please install it and try again."; exit 1; }

# * create the mirror and the filtered repo
# mirror is all of jobtechs repos which might contain documentation
update-mirror:
	#exclude -infra and -infra.wiki
	#	export GHORG_EXCLUDE_MATCH_REGEX='-infra'
	export GHORG_IGNORE_PATH=./ghorgignore
	#!/bin/bash
	i=0
	while true
	do
	    let i++
	    echo "Ghorging try $$i"    
	    timeout 30m $(GHORG)  clone  $(GHORGFLAGS) $(GHORGGITLAB) arbetsformedlingen
	    if [ $$? -eq 124 ]
	    then
	        echo "Ghorg timed out, retrying..."
	        continue
	    fi
	    echo "Sleeping..."
	    sleep 30
	done

# we need a clone of the megadocs theme, some local patches needed atm
SITEDIR=megadocutron

THEME=megadocs
#primary theme targeted

#THEME=hugo-book
# sort of works

#THEME=hugo-theme-bootstrap
# didnt work
$(THEME):
	echo "atm you need to clone $(THEME), and patch it manually, sorry"
	exit 1
$(SITEDIR): $(THEME) filtered
	hugo new site $(SITEDIR)
	root=`pwd`
	cd $$root/$(SITEDIR)/themes
	cp -r $$root/$(THEME) . # some local patches needed
	cd $$root/$(SITEDIR)/content
	ln -s $$root/filtered
	cd $$root/$(SITEDIR)
	echo "theme = '$(THEME)'" >> hugo.toml
	echo "paginate = 1500" >> hugo.toml
	cp $$root/_index.md $$root/$(SITEDIR)/content
# * run hugo
hugo: $(SITEDIR)
	cd $(SITEDIR)
	time hugo

server: $(SITEDIR)
	cd $(SITEDIR)/
	hugo server -v

# * prepare the filtered directory from the mirror
# filtered is a directory of all markdown files, etc, needed to run hugo
# files are copied to filtered from mirror
# then further modified so hugo works

# this is further complicated because i want to maintain https://gitlab.com/arbetsformedlingen/www/megadocutron-content
# which is just a git checkin of the "filtered" dir, atm in filtered.git, then these needs to be synced
filtered: mirror
	rsync -avmq --include='*.md' -f 'hide,! */' mirror filtered
	rsync -avmq --include='*.org' -f 'hide,! */' mirror filtered
	rsync -avmq   --include='*.png' -f 'hide,! */' mirror filtered
	rsync -avmq   --include='*.jpg' -f 'hide,! */' mirror filtered
	#blacklist things too much of a headache atm

	# this is weird md
	#find filtered -type d -name 'jobtechdev-se-v2' -prune -exec rm -rf {} \;
	#find filtered -type d -name 'jobtechdev-se' -prune -exec rm -rf {} \;

	#this is just plain html, should not be
	find filtered -type d -name 'uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-foer-kompetensfoersoerjning-och-livslangt-laerande.wiki' -prune -exec rm -rf {} \;

	# these are artefacts that shouldnt be processed
	find filtered -type d -name '.github' -prune -exec rm -rf {} \;
	find filtered -type d -name '.gitlab' -prune -exec rm -rf {} \;
	find filtered -type d -name 'node_modules' -prune -exec rm -rf {} \; #some of these are checked in by misstake
	find filtered -type d -name '.pc' -prune -exec rm -rf {} \;	#.pc files seems like compiled stuff we dont want also
	# of course we dont want the genereated files messed with again!
	find filtered -type d -name 'megadocutron-content' -prune -exec rm -rf {} \;

        # the following breaks hugo while dealing with megaindex.md, dunno why atm
	find filtered -type d -name 'adl' -prune -exec rm -rf {} \;
	find filtered -type d -name '20.yrkesinfo' -prune -exec rm -rf {} \;
	find filtered -type d -name 'giggi-motivationsmatchning.wiki' -prune -exec rm -rf {} \; # this repo contains quotes in filenames, and i dont feel like dealing with it atm

	# some files are empty, which breaks md parsing, just remove them
	find filtered -type f -empty -delete
filtered.git:
	#atm you need 1st clone https://gitlab.com/arbetsformedlingen/www/megadocutron-content to filtered.git
	exit 1
update-filtered.git: filtered.git
	rsync --exclude='.git' -a --info=NAME0,DEL0  --delete "filtered/" "filtered.git/"
	cd filtered.git/
	git add .
	git commit -m"updated"
	git push

# fix all md files in filtered, so they contain a title block
fixtitleblock: filtered
	bb fixtitleblock.bb  --process-filtered > fixtitleblock.log  2>&1

# lint md files in filtered, and also fix simple errors
lintandfix.txt: filtered
	time podman run -it -v $$PWD:$$PWD:z -v $$PWD/filtered:/workdir:z ghcr.io/igorshubovych/markdownlint-cli:latest --fix --disable MD013 --  '**/*.md' > lintandfix.txt

# * primary targets
all: check-tools filtered fixtitleblock $(SITEDIR) hugo

cleanbuild: clean all


clean:
	rm -rf $(SITEDIR)
	rm -rf filtered 


clean-mirror:
	rm -rf mirror || true

# * status
status:
	gita ll

files:
	find . -name ".git" -prune  -o \( -name "*.org" -o -name "*.md" \) -print

