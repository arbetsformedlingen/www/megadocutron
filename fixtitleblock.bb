#!/usr/bin/env bb

(require '[clojure.string :as str])
(require '[clojure.java.io :as io])
(require '[clj-yaml.core :as yaml])
(require '[markdown.core :as md])
(require '[markdown.transformers :as md.transformers])
(require '[markdown.common :as md.common])


(require '[taoensso.timbre :as timbre])
(require '[clojure.java.io :as io]
         '[clojure.string :as str])


(require '[babashka.fs :as fs])
(require '[babashka.process :as p])
(require '[babashka.pods :as pods])
(require '[babashka.cli :as cli])


;;i want keys in m1 to be preserved, even if theres a corresponding key in m2, because in this case humans have generated m1, while m2 is machine generated
(defn merge-except [m1 m2]
  (merge m1 (apply dissoc m2 (keys m1))))

(defn read-n-lines [filename n]
  (with-open [rdr (io/reader filename)]
    (->> (line-seq rdr)
         (take n)
         (interpose "\n")
         (apply str))))



(defn remove-first-yaml-frontmatter [file]
  (with-open [rdr (io/reader file)]
    (loop [lines (line-seq rdr)
           in-frontmatter false
           passed-frontmatter false
           result '()]
      (let [line (first lines)]
        (cond
          (nil? line) (str/join "\n" (reverse result))
          (and (not in-frontmatter) (not passed-frontmatter) (= "---" line)) 
          (recur (next lines) true passed-frontmatter result)
          (and in-frontmatter (= "---" line)) 
          (recur (next lines) false true result)
          :else 
          (if passed-frontmatter
            (recur (next lines) in-frontmatter passed-frontmatter (cons line result))
            (recur (next lines) in-frontmatter passed-frontmatter result)))))))

(defn read-markdown [path]
  (remove-first-yaml-frontmatter (io/file path)))


;;abuse the md parser to return the 1st heading, as simplification to using the bootleg pod, which crashed a lot, on our bad markdown files

(defn my-heading [text {:keys [buf next-line code codeblock heading-anchors] :as state}]
  (if (md.common/heading-level text) 
    [(str  (md.common/heading-text text) "\n") (assoc state :inline-heading true)]
    ["" state]
    ))
(defn first-title [txt]
  (-> (md/md-to-html-string-with-meta
       txt
       :replacement-transformers  [  my-heading  ])
      :html
      (str/split #"\n")
      first))




;;(first-title    (slurp  "/home/joakim/roles/jobtech/megadocutron/mirror/arbetsformedlingen/www/jobtechdev-se-v2/user/pages/01.home/frontpage.en.md"))
;;(first-title     (slurp "/tmp/tst.md"))
;;(first-title (slurp "/home/joakim/roles/jobtech/megadocutron/filtered/mirror/arbetsformedlingen/documentation/templates/Readme-template-API.md"))
;;(first-title (slurp "filtered/mirror/arbetsformedlingen/individdata/connect-once/af-connect/app/lib/common-cv-model/HROpen-v4.2-1/common/CONTRIBUTORS.md"))
(defn find-first-h1-or-h2 [hickory-seq]
  (when (seq? hickory-seq)
    (let [[tag & more] (first hickory-seq)]
      (cond
        (or (= tag :h1) (= tag :h2))
        [tag more]
        (coll? more)
        (or (find-first-h1-or-h2 more)
            (find-first-h1-or-h2 (rest hickory-seq)))
        :else
        (find-first-h1-or-h2 (rest hickory-seq))))))

(defn count-newlines [s]
  (count (re-seq #"\n" s)))

(defn filtered-to-mirror [filename]
  (str/replace filename "filtered/" "")
  )

;; find out git info about a file
;; file:
;; /home/joakim/roles/jobtech/megadocutron/mirror/arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/uploads/c69ee165122d3a011598cc1c5b51b34c/Digi-general-2023-05-08.png
;; cd ~/roles/jobtech/megadocutron/mirror/arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/uploads/c69ee165122d3a011598cc1c5b51b34c/
;; git rev-parse --show-toplevel
;;$ git log -1 --format="%at" -- Digi-general-2023-05-08.png


(defn git-repo-hierarchy-tags [input] 
  (let [parts (str/split (str/replace input #"^/" "")  #"/")]
    (loop [remaining-parts parts
           acc []]
      (if (first remaining-parts)
        (recur (next remaining-parts)
               (conj acc
                     (str/join "::" remaining-parts 
                               )))
        acc))))

;; (str/join "::" (str/split "a/b/c" #"/"))
;;(str/replace "/asd/nb" #"^/" "")
;; (str "---\n"  (yaml/generate-string {:tags (git-repo-hierarchy-tags "a1/b2/c3/d4/e5/f6") } :dumper-options {:flow-style :block} ) "---\n")
;; (git-repo-hierarchy-tags "/data/report/mentor-api-prod__splitclassvalidation/2d2968565e2f881e19f1f6015457ef6d101be9b6/README.md")
(defn git-info [file]
  (let [dir (str (fs/parent file))
        unix-date (Integer/parseInt (str/trim (:out (p/shell {:out :string :dir dir} "git log -1 --format='%at'" ))))
        date (java.time.LocalDateTime/ofInstant
              (java.time.Instant/ofEpochMilli (* 1000 unix-date))
              (java.time.ZoneId/systemDefault))
        ;;(time/instant (time/milliseconds (* unix-date 1000)))
        date-formatted (.format date   (java.time.format.DateTimeFormatter/ofPattern "yyyy-MM-dd HH:mm:ss"))
        git-branch (str/trim (:out (p/shell {:out :string :dir dir} "git branch'" )))
        ]
    {:git-dir
     (-> (str/trim (:out  (p/shell {:out :string :dir dir} "git  rev-parse --show-toplevel" )))
         (str/replace    (str (fs/cwd)) "")
         (str/replace    "/mirror" "")
         )
     :commit-date-unix
     unix-date
     :commit-date-formatted
     date-formatted
     :git-branch
     git-branch
     })
  )
;;(git-info (fs/file "/home/joakim/roles/jobtech/megadocutron/mirror/arbetsformedlingen/arbetsformedlingen/designsystem/digi.wiki/uploads/c69ee165122d3a011598cc1c5b51b34c/Digi-general-2023-05-08.png"))
;;(git-info "/home/joakim/filtered/mirror/arbetsformedlingen/arbetsformedlingen-security-policy-project/README.md")
(defn process-file [file]
  (let [title (->  (fs/strip-ext file)
                   (str/replace  #"\"" "")
                   (str/replace #"filtered" ""))
        header (read-n-lines file 10)
        entire-file (slurp file)
        parsed-as-yaml (yaml/parse-string header :load-all true)
        no-frontmatter-match (not (re-seq #"^---\n[\s\S]*?\n---\n" header)) ;; begin with ---, then a bunch of random lines, then ---
        broken-frontmatter-match (re-seq #"^---\n(?:(?!title:)([\s\S]+))\n---"  header)
        parsed-as-meta (if (not (= 0 (fs/size file)))
                         (:metadata (md/md-to-html-string-with-meta entire-file))
                         {}) ;;md-to-meta failed for some reason
        file-git-info (git-info (fs/file (filtered-to-mirror (str file))))
        path (str/replace (str file) "filtered/mirror" "")
        new-title (or (first-title entire-file) (fs/strip-ext (fs/file-name file)))
        ;;TODO other tag sources should be joined before this step
        ;;  - the file hierarchy
        ;;  - existing tags in the files (atm theres nothging)
        ;;  - other interesting tag sourvces
        tags (git-repo-hierarchy-tags (str/replace path (:git-dir file-git-info) "")) 
        generated-meta {:title new-title
                        :gitlaburl (str "https://gitlab.com"
                                        (:git-dir file-git-info)
                                        "/-/blob/main/" ;; TODO this can be another branch
                                        (str/replace path (:git-dir file-git-info) "")
                                        )
                        :gitdir (:git-dir file-git-info)
                        :gitdir-file-path
                        (str/replace path (:git-dir file-git-info) "")
                        :date (:commit-date-formatted file-git-info)
                        :path path
                        :tags tags
                        }
        new-meta (merge-except parsed-as-meta generated-meta)
        
        new-meta-as-yaml (str "---\n"  (yaml/generate-string  new-meta :dumper-options {:flow-style :block} ) "---\n")
        new-file-contents (str new-meta-as-yaml (slurp file))
        ]

    ;;(timbre/info no-frontmatter-match)
    ;;(timbre/info header)
    ;;(prn broken-frontmatter-match)

    ;;    (timbre/info parsed-as-yaml)
    (timbre/info "old meta:" parsed-as-meta)
    (timbre/info "new meta:" new-meta-as-yaml)
    (timbre/info "git info:" file-git-info)
    
    new-file-contents))

(defn process-file-in-place [file]
  (spit file (process-file file)))

(defn all-md-files []
  (filter #(= "md" (fs/extension %)) (file-seq (io/file "filtered"))))

;;(file-tree "filtered")
;;(file-tree "markdown-clj")

;;
;;(walk-file-tree f {:keys [:pre-visit-dir :post-visit-dir :visit-file :visit-file-failed :follow-links :max-depth]})

;; this fn is atm a little bit hard to read, but it uses walk-file-tree to visit every directory and every file
;; the number of / in a dir name gives how many # there will be in the md title
;; the number of recursively counted md files in a dir decides if its going to get a # md title at all
;; so its not very efficient, but it takes just a second to create the index, so no big deal, we have worse problems
;; also atm it only considered md files, but ought also consider at least org files
(defn walk-filtered
  ([] (walk-filtered "filtered/mirror/arbetsformedlingen/megaindex.md"))
  ([outfile]
   (timbre/info "make " outfile)
   (let [contents (with-out-str 
                    (fs/walk-file-tree (fs/file "filtered")
                                       {:pre-visit-dir  #(do (if (< 0 (count (fs/glob %1 "**.md")))
                                                               (println (str (-> (re-seq #"/" (str %1))
                                                                                 count
                                                                                 dec
                                                                                 (repeat "#")
                                                                                 str/join) " "
                                                                             (fs/file-name %1)
                                                                             )))
                                                             %2
                                                             
                                                             :continue)
                                        :visit-file #(do (if (= "md" (fs/extension %1 )) (println (str "["  (fs/strip-ext (fs/file-name %1)) "]" "({{< ref \"/"  (str %1) "\" >}} )\n"))) %2 :continue)

                                        }))]
     (spit outfile contents)
     )))
;;(walk-filtered "/tmp/tst.md")
;; (count (fs/glob "filtered/mirror/arbetsformedlingen/" "**.md"))



(defn process-filtered []
  (let  [files (all-md-files)]
    (doseq [file files]
      (timbre/info "------------")
      (timbre/info (str file " : ")) ;; print filename here for now i case of crash
      (process-file-in-place file))
    (timbre/info "------------")
    (walk-filtered)
    (timbre/info "Done!")))

;;didnt find a lib fn to do time formattion
(defn ms-to-human-readable [ms]
  (let [total-secs (int (/ ms 1000))
        secs (mod total-secs 60)
        total-mins (int (/ total-secs 60))
        mins (mod total-mins 60)
        total-hours (int (/ total-mins 60))]
    (str total-hours " hours " mins " minutes " secs " seconds ")))



(defn time-it [f]
  (let [start (System/currentTimeMillis)
        ret (f)
        _ (timbre/info  "Elapsed time: " (str (ms-to-human-readable  (- (System/currentTimeMillis) start)) "ms"))]
    ret))




(defn -main [args]
  (timbre/info "starting")
  (cond (:process-filtered args) (time-it process-filtered)
        (:process-file args)  (println (process-file (:filename args)))
        (:walk-filtered args) (walk-filtered)
        :else (println "read the code please for usage instructions"))

  )

(-main (cli/parse-opts *command-line-args* {:coerce {:image :string}}))
